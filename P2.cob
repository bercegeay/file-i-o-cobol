IDENTIFICATION DIVISION.
PROGRAM-ID.  P2test.
ENVIRONMENT DIVISION.
INPUT-OUTPUT SECTION.
FILE-CONTROL.
    SELECT myInFile  
       ASSIGN TO "P2In.dat"
      organization is line sequential.
    SELECT myOutFile 
       ASSIGN TO "P2Out.dat"
       organization is line sequential.
DATA DIVISION.
FILE SECTION.
FD myInFile.
01 CR.
   05  cCLASSES.
      10  cCourse  PIC X(10).
      10  cTitle  PIC X(28).
      10  cGrade  PIC A.
      10  cEarned  PIC 9.
FD myOutFile.
01  oSELU PIC A(45).
01  oLocation PIC X(35).
01  oName PIC X(10).
01  oW PIC X(8).
01  oSemester PIC X(10).
01  oTitles PIC A(60).

01 outClass.
   05  oCLASSES.
      10  oCourse  PIC X(10).
      10  oTitle  PIC X(28).
      10  oGrade  PIC A.
      10  oOneSpace PIC A.
      10  oEarned  PIC Z9.Z(2).
      10  oThreeSpace PIC A(3).
      10  Quality  PIC Z9.Z(2).
01  eachSemester.
   05  oSemTotal  PIC A(40).
   05  sEarned  PIC Z9.ZZ.
   05  oThreeSpace2 PIC A(3).
   05  sWeighted  PIC Z9.ZZ.
   05  oThreeSpace4 PIC A(3).
   05  oGPA  PIC 9.Z(2).
01  Cumulative.
   05  oCumTotal  PIC A(40).
   05  cuEarned  PIC Z9.ZZ VALUE ZEROS.
   05  oThreeSpace3 PIC A(3).
   05  cuWeighted  PIC Z9.ZZ VALUE ZEROS.
   05  oThreeSpace5 PIC A(3).
   05  ocuGPA  PIC 9.ZZ.
01 BLANK-LINES PIC A VALUE SPACES.

WORKING-STORAGE SECTION.
01 a  PIC  X(3)  VALUE "YES".
01 b  PIC  X(3)  VALUE "YES".
01 wSELU PIC X(45) VALUE "            SOUTHEASTERN LOUISIANA UNIVERSITY".
01 wLocation PIC X(35) VALUE "                  Hammond, LA 70402".
01 wName PIC X(10) VALUE "JOHN SMITH".
01 wW PIC X(8) VALUE "W1234567".
01 wSemester PIC X(10) VALUE "FALL 2014".
01 wTitles PIC X(62) VALUE "COURSE   TITLE                        GR EARNED QPTS".
01 wSemTotal PIC A(40) VALUE "                             Semester   ".
01 wCumTotal PIC A(40) VALUE "                             Cumulative ".
01 Blankline PIC A VALUE SPACES.
01 ThreeSpace PIC A(3) VALUE SPACES.
01 WS.
   05  wCLASSES.
      10  wCourse  PIC X(10).
      10  wTitle  PIC X(28).
      10  wGrade  PIC A.
      10  wEarned  PIC 9.
   05  wSemesterTotal.
      10  wMultiplier  PIC 9.
      10  wStotal  PIC 99.
      10  wQuality PIC 99.
      10  wWeighted  PIC 99.
      10  wcuEarned  PIC 99.
      10  wcuWeighted  PIC 99.
   05  comparison.
      10  gradeA  PIC A VALUE "A".
      10  gradeB  PIC A VALUE "B".
      10  gradeC  PIC A VALUE "C".

PROCEDURE DIVISION.
    OPEN INPUT myInFile.
    OPEN OUTPUT myOutFile.
    PERFORM writeHeader.
    PERFORM displayHeader.
    PERFORM 4 TIMES
       PERFORM moveClass
    END-PERFORM.
    PERFORM displaySemester.
    PERFORM displayCumulative.
    DISPLAY "SPRING 2015".
    PERFORM 3 TIMES
       PERFORM moveClass
    END-PERFORM.
    PERFORM displaySemester.
    PERFORM displayCumulative.
    CLOSE myInFile.
    CLOSE myOutFile.
    STOP RUN.

readHeader.
   READ myInFile
      AT END
         MOVE "NO" TO a
   END-READ.

writeHeader.
   MOVE wSELU to oSELU.
   WRITE oSELU.
   MOVE wLocation to oLocation.
   WRITE oLocation.
   MOVE Blankline to BLANK-LINES.
   WRITE BLANK-LINES.
   MOVE wName to oName.
   WRITE oName.
   MOVE wW to oW.
   WRITE oW.
   MOVE wSemester to oSemester.
   WRITE oSemester.
   MOVE wTitles to oTitles.
   WRITE oTitles.

displayHeader.
   DISPLAY wSELU.
   DISPLAY wLocation.
   DISPLAY wName.
   DISPLAY wW.
   DISPLAY wSemester.
   DISPLAY wTitles.

moveClass.
   READ myInFile INTO wClasses
      NOT AT END
         MOVE wCourse to oCourse
         MOVE wTitle to oTitle
         MOVE wGrade to oGrade
         MOVE Blankline to oOneSpace
         IF oGrade IS EQUAL gradeA THEN
            MOVE 4 to wMultiplier
         END-IF
         IF oGrade IS EQUAL gradeB THEN
            MOVE 3 to wMultiplier
         END-IF
         IF oGrade IS EQUAL gradeC THEN
            MOVE 2 to wMultiplier
         END-IF
         COMPUTE wQuality = wEarned * wMultiplier
         COMPUTE wWeighted = wWeighted + wQuality
         COMPUTE wStotal = wStotal + wEarned
         MOVE wQuality to Quality
         MOVE ThreeSpace to oThreeSpace
         MOVE wEarned to oEarned
         WRITE outClass
         PERFORM displayClass. 

displayClass.
   DISPLAY oCourse, oTitle, oGrade," ", oEarned, "   ", Quality.

displaySemester.
   MOVE wStotal to sEarned.
   MOVE wWeighted to sWeighted.
   MOVE ThreeSpace to oThreeSpace2.
   MOVE wSemTotal to oSemTotal.
   MOVE ThreeSpace to oThreeSpace4.
   COMPUTE oGPA = wWeighted / wStotal.
   DISPLAY "                      SEMESTER          ",  sEarned , "   ", sWeighted, "   ", oGPA.
   WRITE eachSemester.

   
displayCumulative.
   MOVE sEarned to wStotal.
   MOVE sWeighted to wWeighted.
   COMPUTE wcuEarned = wcuEarned + wStotal.
   COMPUTE wcuWeighted = wcuWeighted + wWeighted.
   MOVE wcuEarned to cuEarned.
   MOVE wcuWeighted to cuWeighted.
   MOVE ThreeSpace to oThreeSpace3.
   MOVE wCumTotal to oCumTotal.
   MOVE ThreeSpace to oThreeSpace5.
   COMPUTE ocuGPA = wcuWeighted / wcuEarned.
   DISPLAY "                      CUMULATIVE        ",  cuEarned , "   ", cuWeighted, "   ", ocuGPA.
   WRITE Cumulative.
   MOVE 0 to wStotal.
   MOVE 0 to wWeighted.

